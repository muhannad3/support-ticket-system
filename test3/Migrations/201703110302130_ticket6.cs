namespace test3.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ticket6 : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Replies", "userId", c => c.Guid(nullable: false));
            AlterColumn("dbo.Tickets", "userId", c => c.Guid(nullable: false));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Tickets", "userId", c => c.String());
            AlterColumn("dbo.Replies", "userId", c => c.String());
        }
    }
}
