namespace test3.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class reply : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Replies", "ticketId", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Replies", "ticketId", c => c.String());
        }
    }
}
