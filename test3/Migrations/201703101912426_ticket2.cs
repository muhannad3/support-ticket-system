namespace test3.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ticket2 : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Tickets",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        content = c.String(),
                        subject = c.String(),
                        userId = c.Int(nullable: false),
                        section = c.String(),
                        date = c.DateTime(nullable: false),
                        status = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.Tickets");
        }
    }
}
