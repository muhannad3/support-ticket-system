namespace test3.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ticket5 : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Replies", "ticketId", c => c.String());
            AlterColumn("dbo.Replies", "userId", c => c.String());
            AlterColumn("dbo.Tickets", "userId", c => c.String());
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Tickets", "userId", c => c.Int(nullable: false));
            AlterColumn("dbo.Replies", "userId", c => c.Int(nullable: false));
            AlterColumn("dbo.Replies", "ticketId", c => c.Int(nullable: false));
        }
    }
}
