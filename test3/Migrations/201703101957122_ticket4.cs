namespace test3.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ticket4 : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Replies",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        ticketId = c.Int(nullable: false),
                        content = c.String(),
                        userId = c.Int(nullable: false),
                        date = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.Replies");
        }
    }
}
