﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using test3.Models;
using Microsoft.AspNet.Identity;

namespace test3.Controllers
{
    public class TicketsController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: Tickets
        public ActionResult Index()
        {
            var manager = new UserManager<ApplicationUser>(new Microsoft.AspNet.Identity.EntityFramework.UserStore<ApplicationUser>(new ApplicationDbContext()));
            var currentUser = manager.FindById(User.Identity.GetUserId());
            var user = getUserId();
            var role = currentUser.role;
            if (role.CompareTo("admin") == 0)
            {
                return View(db.Ticket.ToList());
            }
            else if (role.CompareTo("client") == 0)
            {
                return View(db.Ticket.Where(model => model.userId == user).ToList());
            }
            return View("/Account/Register");

        }

        // GET: Tickets/Details/5
        public ActionResult Details(int? id)
        {
            
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Ticket ticket = db.Ticket.Find(id);
            ViewBag.ticketId = ticket.Id;
            ViewBag.userId = getUserId();
            ViewData["ticketId"] = ticket.Id;
            ViewData["userId"] = getUserId();
            if (ticket == null)
            {
                return HttpNotFound();
            }
            return View(ticket);
        }

        // GET: Tickets/Create
        public ActionResult Create()
        {
            ViewData["userId"] = getUserId();
            var manager = new UserManager<ApplicationUser>(new Microsoft.AspNet.Identity.EntityFramework.UserStore<ApplicationUser>(new ApplicationDbContext()));
            var currentUser = manager.FindById(User.Identity.GetUserId());
            var role = currentUser.role;
            ViewData["role"] = role;
            return View();
        }

        // POST: Tickets/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,content,subject,userId,section,date,status")] Ticket ticket)
        {
            var manager = new UserManager<ApplicationUser>(new Microsoft.AspNet.Identity.EntityFramework.UserStore<ApplicationUser>(new ApplicationDbContext()));
            var currentUser = manager.FindById(User.Identity.GetUserId());
            var role = currentUser.role;
            if (role.ToString().CompareTo("admin") == 0)
            {
                var email = Request.Form["email"];
                var currentUser2 = manager.FindByEmail(email);
                if (currentUser2 != null)
                {
                    ticket.userId = new Guid(currentUser2.Id);
                }else
                {
                    TempData["error"] = "true";
                    return RedirectToAction("/Create");
                }
                
            }
            if (ModelState.IsValid)
            {
                db.Ticket.Add(ticket);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(ticket);
        }

        // GET: Tickets/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Ticket ticket = db.Ticket.Find(id);
            if (ticket == null)
            {
                return HttpNotFound();
            }
            return View(ticket);
        }

        // POST: Tickets/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,content,subject,userId,section,date,status")] Ticket ticket)
        {
            if (ModelState.IsValid)
            {
                db.Entry(ticket).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(ticket);
        }

        // GET: Tickets/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Ticket ticket = db.Ticket.Find(id);
            if (ticket == null)
            {
                return HttpNotFound();
            }
            return View(ticket);
        }

        // POST: Tickets/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Ticket ticket = db.Ticket.Find(id);
            
            var array = db.Reply.Where(model => model.ticketId == ticket.Id).ToList();
            foreach (Reply model in array)
            {
                db.Reply.Remove(model);
                db.SaveChanges();
            }
            db.Ticket.Remove(ticket);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        public Guid getUserId()
        {
            return new Guid(User.Identity.GetUserId());
        }


        [Route("Tickets/createReply")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult createReply([Bind(Exclude = "Id")] Reply reply)
        {
            var jsonData = Json(reply, JsonRequestBehavior.AllowGet);
            ModelState.Remove("Id");
            if (ModelState.IsValid)
            {
                db.Reply.Add(reply);
                db.SaveChanges();

            }
            return Json(reply, JsonRequestBehavior.AllowGet);
        }
        [Route("Tickets/Reply")]
        [HttpGet]
        public ActionResult Reply()
        {
            var ticketId = Request.QueryString["ticketId"];
            int idTicket = Convert.ToInt32(ticketId);
            var user = db.Reply.Where(model => model.ticketId == idTicket).ToList();
            var jsonData = Json(user, JsonRequestBehavior.AllowGet);
            return jsonData;
        }
    }
}
