﻿
var someStringValue = '@(safeStringValue)';
$(document).ready(function () {
    getReplies(ticketId);
});
// Set title for new Reply
$("#btnReply").click(function () {
    $("#title").text("New Reply");
})

// Close modal
$("#btnClose").click(function () {
    clear();
});

// Clear all items
function clear() {
    $("#ticketId").val("");
    $("#userId").val("");
    $("#date").val("");
    $("#content").val("");
}
AddAntiForgeryToken = function (data) {
    data.__RequestVerificationToken = $('#__AjaxAntiForgeryForm input[name=__RequestVerificationToken]').val();
    return data;
};
// save reply
$("#btnSave").click(function (e) {
    var data = {
        ticketId: ticketId,
        userId: userId,
        date: convertDate(date).toUTCString(),
        content: $("#content").val()
    }

    $.ajax({
        url: '/Tickets/createReply',
        type: 'POST',
        dataType: 'json',
        data : AddAntiForgeryToken(data),
        success: function (data) {

            getReplies(ticketId);
            $("#ReplyModal").modal('hide');
            clear();
        },
        error: function (err) {
            alert("Error: " + err.responseText);
        }
    });
});

function getReplies(ticketId) {

    $.ajax({
        url: "/Tickets/Reply?ticketId=" + ticketId,
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            $("#table").html('');
            var trContent = '';
            for (var i = 0; i < data.length; i++) {
                var dt = convertDate(data[i].date);
                trContent += '<tr><th class="control-label col-md-2">' + data[i].content + '</th>' + '<th class="control-label col-md-2">' + dt.toUTCString() + '</th><tr/>';
            }
            $("#table").append(trContent);
        },
        error: function (xhr, status) {
            alert(status);
        }
    });
}
function convertDate(val) {
    var pattern = /Date\(([^)]+)\)/;
    var results = pattern.exec(val);
    return new Date(parseFloat(results[1]));
}
// refresh
$(function () {
    $("#btn").click(function () {
        getReplies(ticketId);
    });
});