﻿using System.Data.Entity;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using System;

namespace test3.Models
{
    // You can add profile data for the user by adding more properties to your ApplicationUser class, please visit http://go.microsoft.com/fwlink/?LinkID=317594 to learn more.
    public class ApplicationUser : IdentityUser
    {
        public string role { get; set; }
        public object Ticket { get; internal set; }

        public async Task<ClaimsIdentity> GenerateUserIdentityAsync(UserManager<ApplicationUser> manager)
        {
            // Note the authenticationType must match the one defined in CookieAuthenticationOptions.AuthenticationType
            var userIdentity = await manager.CreateIdentityAsync(this, DefaultAuthenticationTypes.ApplicationCookie);
            // Add custom user claims here
            return userIdentity;
        }
    }
    public class Ticket
    {
        public int Id { get; set; }
        public string content { get; set; }
        public string subject { get; set; }
        public Guid userId { get; set; }
        public string section { get; set; }
        public DateTime date { get; set; }
        public string status { get; set; }

    }
    public class Reply
    {
        public int Id { get; set; }
        public int ticketId { get; set; }
        public string content { get; set; }
        public Guid userId { get; set; }
        public DateTime date { get; set; }

    }
    public class ApplicationDbContext : IdentityDbContext<ApplicationUser>
    {
        public ApplicationDbContext()
            : base("DefaultConnection", throwIfV1Schema: false)
        {
        }

        public static ApplicationDbContext Create()
        {
            return new ApplicationDbContext();
        }
        public System.Data.Entity.DbSet<Ticket> Ticket { get; set; }
        public System.Data.Entity.DbSet<Reply> Reply { get; set; }
    }
}